/*
 * add-to-eds.c
 *
 * Author:
 *   Christophe Fergeau <teuf@gnome.org>
 *
 * Copyright (C) 2010 Christophe Fergeau
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* e_book_get_contacts to lookup a contact by name or email */
#include <glib.h>
#include <camel/camel.h>
#include <libebook/e-book.h>

#include "mail-parser.h"

GQuark ate_ebook_error_quark (void)
{
    return g_quark_from_static_string ("ate-ebook-error-quark");
}
#define ATE_EBOOK_ERROR ate_ebook_error_quark()

enum _AteEbookError {
    ATE_EBOOK_ERROR_CONTACT
};
typedef enum _AteEbookError AteEbookError;

static EContact *find_contact (EBook *ebook, EContactField field_id, const char *value)
{
    EBookQuery *query;
    char *query_str;
    GError *error = NULL;
    GList *contacts;
    EContact *contact;

    if ((value == NULL) || (*value == '\0')) {
        /* Nothing to search for */
        return NULL;
    }

    contact = NULL;
    query_str = g_strdup_printf ("(is \"%s\" \"%s\")",
                                 e_contact_field_name (field_id), value);

    query = e_book_query_from_string (query_str);
    g_free (query_str);
    if (query == NULL) {
        return NULL;
    }

    e_book_get_contacts (ebook, query, &contacts, &error);
    e_book_query_unref (query);
    if (error) {
        g_print ("Query failed: %s\n", error->message);
        return NULL;
    }

    if (contacts != NULL) {
        /* arbitrarily pick the 1st contact we found */
        contact = g_object_ref (G_OBJECT(contacts->data));
        g_list_foreach (contacts, (GFunc)g_object_unref, NULL);
        g_list_free (contacts);
    }

    return contact;
}

static EContact *create_contact (const char *name, const char *email)
{
    EContact *contact;

    if ((name == NULL) || (email == NULL)) {
        return NULL;
    }
    contact = e_contact_new ();
    e_contact_set (contact, E_CONTACT_FULL_NAME, name);
    e_contact_set (contact, E_CONTACT_EMAIL_1, email);

    return contact;
}

static EBook *open_address_book (GError **error)
{
    EBook *ebook;

    ebook = e_book_new_default_addressbook (error);
    if ((error != NULL) && (*error != NULL)) {
        g_prefix_error (error, "Couldn't open default address book: ");
        return NULL;
    }

    e_book_open (ebook, TRUE, error);
    if ((error != NULL) && (*error != NULL)) {
        g_prefix_error (error, "Couldn't open default address book: ");
        g_object_unref (ebook);
        return NULL;
    }

    return ebook;
}

static gboolean add_to_address_book (EBook *ebook, EContact *contact,
                                     GError **error)
{
    e_book_add_contact (ebook, contact, error);
    if ((error != NULL) && (*error != NULL)) {
        g_prefix_error (error, "Failed to add contact to address book: ");
        return FALSE;
    }

    return TRUE;
}

/* returns TRUE if a contact with the same name/email was found
 * returns FALSE if a new contact needs to be created
 */
static gboolean handle_duplicate_contact (EBook *ebook, const char *name, const char *email)
{
    EContact *contact;

    contact = find_contact (ebook, E_CONTACT_EMAIL, email);
    if (contact != NULL) {
        g_object_unref (contact);
        return TRUE;
    }

    contact = find_contact (ebook, E_CONTACT_FULL_NAME, name);
    if (contact != NULL) {
        GList *emails;
        g_print ("Updated existing contact %s with new email %s\n",
                 name, email);
        emails = e_contact_get (contact, E_CONTACT_EMAIL);
        emails = g_list_append (emails, g_strdup (email));
        e_contact_set (contact, E_CONTACT_EMAIL, emails);
        g_list_foreach (emails, (GFunc)g_free, NULL);
        g_list_free (emails);
        e_book_commit_contact (ebook, contact, NULL);
        return TRUE;
    }

    return FALSE;
}

static gboolean add_contact_for (EBook *ebook, const char *name, const char *email, GError **error)
{
    EContact *contact;

    /* First, update existing contact if we already know about this guy */
    if (handle_duplicate_contact (ebook, name, email)) {
        return TRUE;
    }

    /* If we couldn't find an existing contact to update, create a new one */
    contact = create_contact (name, email);
    if (contact == NULL) {
        g_set_error (error, ATE_EBOOK_ERROR, ATE_EBOOK_ERROR_CONTACT,
                     "Failed to create contact for %s <%s>\n", name, email);
        return FALSE;
    }

    add_to_address_book (ebook, contact, error);
    g_object_unref (G_OBJECT (contact));
    if ((error != NULL) && (*error != NULL)) {
        g_prefix_error (error, "Failed to add to address book: ");
        return FALSE;
    }
    g_print ("Added new contact %s <%s> to address book\n", name, email);
    return TRUE;
}

int main (int argc, char **argv)
{
    GError *error = NULL;
    EBook *ebook = NULL;
    char *name = NULL;
    char *email = NULL;
    int exit_code = 0;

    g_type_init ();
    camel_init (NULL, FALSE);

    ate_parser_get_name_and_email (argv[1], &name, &email, &error);
    if (error != NULL) {
        g_print ("Failed to get name and email from message: %s\n", error->message);
        exit_code = 1;
        goto out;
    }

    ebook = open_address_book (&error);
    if (error != NULL) {
        g_print ("Failed to open address book: %s\n", error->message);
        exit_code = 2;
        goto out;
    }
    add_contact_for (ebook, name, email, &error);

out:
    if (name != NULL) {
        g_free (name);
    }
    if (email != NULL) {
        g_free (email);
    }
    if (ebook != NULL) {
        g_object_unref (ebook);
    }

    return exit_code;
}
