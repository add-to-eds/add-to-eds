/*
 * mail-parser.c
 *
 * Author:
 *   Christophe Fergeau <teuf@gnome.org>
 *
 * Copyright (C) 2010 Christophe Fergeau
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
#include <libedataserver/eds-version.h>
#include <camel/camel.h>
#include "mail-parser.h"

#if EDS_CHECK_VERSION(2,32,0)
#define camel_object_unref(obj) g_object_unref(G_OBJECT(obj))
#endif

GQuark ate_parser_error_quark (void)
{
    return g_quark_from_static_string ("ate-parser-error-quark");
}

enum _AteParserError {
    ATE_PARSER_ERROR_IO,
    ATE_PARSER_ERROR_MESSAGE
};
typedef enum _AteParserError AteParserError;

void ate_parser_get_name_and_email (const char *filename,
                                    char **name, char **email,
                                    GError **error)
{
    CamelStream *stream = NULL;
    CamelMimeMessage *message = NULL;
    CamelInternetAddress *from = NULL;
    int result;

    if (filename == NULL) {
        stream = camel_stream_fs_new_with_fd (fileno(stdin));
    } else {
#if EDS_CHECK_VERSION(2,32,0)
        stream = camel_stream_fs_new_with_name (filename, O_RDONLY, 0, error);
#else
        stream = camel_stream_fs_new_with_name (filename, O_RDONLY, 0);
#endif
    }
    if (stream == NULL) {
#if EDS_CHECK_VERSION(2,32,0)
        g_prefix_error (error, "Couldn't open %s: ", filename);
#else
        g_set_error (error, ATE_PARSER_ERROR, ATE_PARSER_ERROR_IO,
                     "Couldn't open %s", filename);
#endif
        goto out;
    }

    message = camel_mime_message_new ();
#if EDS_CHECK_VERSION(2,32,0)
    result = camel_data_wrapper_construct_from_stream((CamelDataWrapper *)message, stream, error);
#else
    result = camel_data_wrapper_construct_from_stream((CamelDataWrapper *)message, stream);
#endif
    if (result == -1) {
#if EDS_CHECK_VERSION(2,32,0)
        g_prefix_error (error, "Failed to parse message: ");
#else
        g_set_error (error, ATE_PARSER_ERROR, ATE_PARSER_ERROR_MESSAGE,
                     "Failed to parse message");
#endif
        goto out;
    }

    from = camel_mime_message_get_from (message);
    if ((from == NULL) || (camel_address_length ((CamelAddress *)from) < 1)){
        g_set_error (error, ATE_PARSER_ERROR, ATE_PARSER_ERROR_MESSAGE,
                     "Empty From:");
        goto out;
    }

    result = camel_internet_address_get (from, 0, (const char **)name,
                                         (const char **)email);
    if (result == FALSE) {
        g_set_error (error, ATE_PARSER_ERROR, ATE_PARSER_ERROR_MESSAGE,
                     "Couldn't get name and address");
        goto out;
    }

    /* name and email belong to the 'from' object so we have to make sure
     * they don't get away when it gets destroyed */
    if (name != NULL) {
        *name = g_strdup (*name);
    }
    if (email != NULL) {
        *email = g_strdup (*email);
    }


out:
    if (message != NULL) {
        camel_object_unref (message);
    }
    if (stream != NULL) {
        camel_object_unref (stream);
    }
}
